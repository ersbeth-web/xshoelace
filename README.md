# xShoelace

Additional web components based on  [Shoelace](https://shoelace.style/).

## Installation
```  npm install @ersbeth/xshoelace```

## Usage

### Web components

This library provides the following web components:

* `sx-markdown`
* `sx-shell`
* `sx-shell-button`
* `sx-page`

### CSS

This library provides a global stylesheet in `dist/css/global.css`

### Icons

This library provides a function to set the default icon folder:

```js
import { registerIcons } from "@ersbeth/xshoelace"
registerIcons("./icons");
```

### Forms

This library provides better form handling:

```js
// LIT ELEMENT
import { LitElement, html } from 'lit';
import { customElement,  } from 'lit/decorators.js';

// SHOELACE
import '@shoelace-style/shoelace/dist/components/input/input.js';
import '@shoelace-style/shoelace/dist/components/button/button.js';

// X-SHOELACE
import { extendForms, type SxSubmitEvent } from "@ersbeth/xshoelace"

@customElement('x-elem')
export class Elem extends LitElement {

    protected updated(): void { extendForms(this.shadowRoot!); }

    submit(e: SxSubmitEvent) {
        console.log(e.detail);
    }

    protected render = () => {
        return html`
            <form id="sxForm" @sx-submit=${this.submit}>
                <sl-input name="input1" label="input1"></sl-input>
            </form>
            <sl-button type="submit" form="sxForm">Submit</sl-button>
        `
    }
}

const element = document.createElement('x-custom');
document.body.appendChild(element);
```



