import { serialize } from '@shoelace-style/shoelace/dist/utilities/form.js';
import { html, css, LitElement } from 'lit';
import { customElement, property, query, queryAssignedElements } from 'lit/decorators.js';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';
import { marked } from 'marked';
import { when } from 'lit/directives/when.js';
import { event, mediaQuery } from '@ersbeth/xlit';
import { styleMap } from 'lit/directives/style-map.js';
import { registerIconLibrary } from '@shoelace-style/shoelace/dist/utilities/icon-library.js';

/**
 * Extends forms under the given shadowRoot so that they raise SxSubmitEvents.
 *
 * For now it should be called in "updated" callback until this issue is fixed:
 * https://github.com/shoelace-style/shoelace/issues/718
 *
 * TLDR: onFormSubmit must be added after all form inputs are added to DOM
 * Once resolved we should either move this in a mutation observer or use directly onSubmit in forms
 */
function extendForms(root) {
    const forms = root.querySelectorAll("form");
    forms.forEach(form => form.removeEventListener("submit", onSubmit));
    forms.forEach(form => form.addEventListener("submit", onSubmit));
}
/**
 * This function prevents default form sending behaviour (page reload) and forwards form data as JS object.
 * @param event Submit event
 */
function onSubmit(event) {
    event.preventDefault();
    const form = event.target;
    const data = serialize(form);
    form.dispatchEvent(new CustomEvent("sx-submit", { detail: data }));
}

/******************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise, SuppressedError, Symbol */


function __esDecorate(ctor, descriptorIn, decorators, contextIn, initializers, extraInitializers) {
    function accept(f) { if (f !== void 0 && typeof f !== "function") throw new TypeError("Function expected"); return f; }
    var kind = contextIn.kind, key = kind === "getter" ? "get" : kind === "setter" ? "set" : "value";
    var target = !descriptorIn && ctor ? contextIn["static"] ? ctor : ctor.prototype : null;
    var descriptor = descriptorIn || (target ? Object.getOwnPropertyDescriptor(target, contextIn.name) : {});
    var _, done = false;
    for (var i = decorators.length - 1; i >= 0; i--) {
        var context = {};
        for (var p in contextIn) context[p] = p === "access" ? {} : contextIn[p];
        for (var p in contextIn.access) context.access[p] = contextIn.access[p];
        context.addInitializer = function (f) { if (done) throw new TypeError("Cannot add initializers after decoration has completed"); extraInitializers.push(accept(f || null)); };
        var result = (0, decorators[i])(kind === "accessor" ? { get: descriptor.get, set: descriptor.set } : descriptor[key], context);
        if (kind === "accessor") {
            if (result === void 0) continue;
            if (result === null || typeof result !== "object") throw new TypeError("Object expected");
            if (_ = accept(result.get)) descriptor.get = _;
            if (_ = accept(result.set)) descriptor.set = _;
            if (_ = accept(result.init)) initializers.unshift(_);
        }
        else if (_ = accept(result)) {
            if (kind === "field") initializers.unshift(_);
            else descriptor[key] = _;
        }
    }
    if (target) Object.defineProperty(target, contextIn.name, descriptor);
    done = true;
}
function __runInitializers(thisArg, initializers, value) {
    var useValue = arguments.length > 2;
    for (var i = 0; i < initializers.length; i++) {
        value = useValue ? initializers[i].call(thisArg, value) : initializers[i].call(thisArg);
    }
    return useValue ? value : void 0;
}
typeof SuppressedError === "function" ? SuppressedError : function (error, suppressed, message) {
    var e = new Error(message);
    return e.name = "SuppressedError", e.error = error, e.suppressed = suppressed, e;
};

/**
 * This element provides a markdown renderer.
 */
let SxMarkdown = (() => {
    let _classDecorators = [customElement('sx-markdown')];
    let _classDescriptor;
    let _classExtraInitializers = [];
    let _classThis;
    let _classSuper = LitElement;
    let _content_decorators;
    let _content_initializers = [];
    let _content_extraInitializers = [];
    (class extends _classSuper {
        static { _classThis = this; }
        static {
            const _metadata = typeof Symbol === "function" && Symbol.metadata ? Object.create(_classSuper[Symbol.metadata] ?? null) : void 0;
            _content_decorators = [property({ type: String })];
            __esDecorate(this, null, _content_decorators, { kind: "accessor", name: "content", static: false, private: false, access: { has: obj => "content" in obj, get: obj => obj.content, set: (obj, value) => { obj.content = value; } }, metadata: _metadata }, _content_initializers, _content_extraInitializers);
            __esDecorate(null, _classDescriptor = { value: _classThis }, _classDecorators, { kind: "class", name: _classThis.name, metadata: _metadata }, null, _classExtraInitializers);
            _classThis = _classDescriptor.value;
            if (_metadata) Object.defineProperty(_classThis, Symbol.metadata, { enumerable: true, configurable: true, writable: true, value: _metadata });
        }
        #content_accessor_storage = __runInitializers(this, _content_initializers, "");
        // API
        /**
         * Markdown string to be rendered
         */
        get content() { return this.#content_accessor_storage; }
        set content(value) { this.#content_accessor_storage = value; }
        // INTERNAL
        get unsafeHtml() { return unsafeHTML(marked.parse(this.content)); }
        // TEMPLATE
        render = (__runInitializers(this, _content_extraInitializers), () => html `${this.unsafeHtml}`);
        // STYLE
        static styles = [
            css `
        em  {
            color: var(--sl-color-primary-700);
        }

        strong {
            color: var(--sl-color-danger-600);
        }

        a {
            color: var(--sl-color-primary-500);
        }

        li {
            margin-bottom: 0.5rem;
        }

        p,li{
            line-height: 1.3em;
            font-size: 0.9rem;        
        }

        h1 {
            font-size: 1rem;
            color: var(--sl-color-primary-500);
            margin-block: 1.5rem 0.7rem;
            margin-inline: 0px;
            font-weight: bold;       
        }

        h2 {
            font-size: 0.8rem;
            color: var(--sl-color-primary-600);
            margin-block: 1rem 0.5rem;
            margin-inline: 0px;
            font-weight: bold;       
        }
        `
        ];
        static {
            __runInitializers(_classThis, _classExtraInitializers);
        }
    });
    return _classThis;
})();

let SxShell = (() => {
    let _classDecorators = [customElement('sx-shell')];
    let _classDescriptor;
    let _classExtraInitializers = [];
    let _classThis;
    let _classSuper = LitElement;
    let _logo_decorators;
    let _logo_initializers = [];
    let _logo_extraInitializers = [];
    let _current_decorators;
    let _current_initializers = [];
    let _current_extraInitializers = [];
    let _sxCurrentUpdated_decorators;
    let _sxCurrentUpdated_initializers = [];
    let _sxCurrentUpdated_extraInitializers = [];
    let _portrait_decorators;
    let _portrait_initializers = [];
    let _portrait_extraInitializers = [];
    let _menu_decorators;
    let _menu_initializers = [];
    let _menu_extraInitializers = [];
    let _menuButtons_decorators;
    let _menuButtons_initializers = [];
    let _menuButtons_extraInitializers = [];
    (class extends _classSuper {
        static { _classThis = this; }
        static {
            const _metadata = typeof Symbol === "function" && Symbol.metadata ? Object.create(_classSuper[Symbol.metadata] ?? null) : void 0;
            _logo_decorators = [property({ type: String })];
            _current_decorators = [property({ type: String, })];
            _sxCurrentUpdated_decorators = [event("sx-current-updated")];
            _portrait_decorators = [mediaQuery("(orientation: portrait)")];
            _menu_decorators = [query("#nav-menu")];
            _menuButtons_decorators = [queryAssignedElements({ slot: "menu" })];
            __esDecorate(this, null, _logo_decorators, { kind: "accessor", name: "logo", static: false, private: false, access: { has: obj => "logo" in obj, get: obj => obj.logo, set: (obj, value) => { obj.logo = value; } }, metadata: _metadata }, _logo_initializers, _logo_extraInitializers);
            __esDecorate(this, null, _current_decorators, { kind: "accessor", name: "current", static: false, private: false, access: { has: obj => "current" in obj, get: obj => obj.current, set: (obj, value) => { obj.current = value; } }, metadata: _metadata }, _current_initializers, _current_extraInitializers);
            __esDecorate(this, null, _menu_decorators, { kind: "accessor", name: "menu", static: false, private: false, access: { has: obj => "menu" in obj, get: obj => obj.menu, set: (obj, value) => { obj.menu = value; } }, metadata: _metadata }, _menu_initializers, _menu_extraInitializers);
            __esDecorate(this, null, _menuButtons_decorators, { kind: "accessor", name: "menuButtons", static: false, private: false, access: { has: obj => "menuButtons" in obj, get: obj => obj.menuButtons, set: (obj, value) => { obj.menuButtons = value; } }, metadata: _metadata }, _menuButtons_initializers, _menuButtons_extraInitializers);
            __esDecorate(null, null, _sxCurrentUpdated_decorators, { kind: "field", name: "sxCurrentUpdated", static: false, private: false, access: { has: obj => "sxCurrentUpdated" in obj, get: obj => obj.sxCurrentUpdated, set: (obj, value) => { obj.sxCurrentUpdated = value; } }, metadata: _metadata }, _sxCurrentUpdated_initializers, _sxCurrentUpdated_extraInitializers);
            __esDecorate(null, null, _portrait_decorators, { kind: "field", name: "portrait", static: false, private: false, access: { has: obj => "portrait" in obj, get: obj => obj.portrait, set: (obj, value) => { obj.portrait = value; } }, metadata: _metadata }, _portrait_initializers, _portrait_extraInitializers);
            __esDecorate(null, _classDescriptor = { value: _classThis }, _classDecorators, { kind: "class", name: _classThis.name, metadata: _metadata }, null, _classExtraInitializers);
            _classThis = _classDescriptor.value;
            if (_metadata) Object.defineProperty(_classThis, Symbol.metadata, { enumerable: true, configurable: true, writable: true, value: _metadata });
        }
        #logo_accessor_storage = __runInitializers(this, _logo_initializers, void 0);
        /**
         * Path to the logo image
         */
        get logo() { return this.#logo_accessor_storage; }
        set logo(value) { this.#logo_accessor_storage = value; }
        #current_accessor_storage = (__runInitializers(this, _logo_extraInitializers), __runInitializers(this, _current_initializers, void 0));
        /**
         * Current button id
         */
        get current() { return this.#current_accessor_storage; }
        set current(value) { this.#current_accessor_storage = value; }
        sxCurrentUpdated = (__runInitializers(this, _current_extraInitializers), __runInitializers(this, _sxCurrentUpdated_initializers, void 0));
        // INTERNAL
        portrait = (__runInitializers(this, _sxCurrentUpdated_extraInitializers), __runInitializers(this, _portrait_initializers, void 0));
        #menu_accessor_storage = (__runInitializers(this, _portrait_extraInitializers), __runInitializers(this, _menu_initializers, void 0));
        get menu() { return this.#menu_accessor_storage; }
        set menu(value) { this.#menu_accessor_storage = value; }
        #menuButtons_accessor_storage = (__runInitializers(this, _menu_extraInitializers), __runInitializers(this, _menuButtons_initializers, void 0));
        get menuButtons() { return this.#menuButtons_accessor_storage; }
        set menuButtons(value) { this.#menuButtons_accessor_storage = value; }
        get pos() {
            const button = this.menuButtons.find(element => element.current);
            const absolutePos = (this.portrait ? button?.getBoundingClientRect().left : button?.getBoundingClientRect().top) ?? 0;
            const referencePos = (this.portrait ? this.menu?.getBoundingClientRect().left : this.menu?.getBoundingClientRect().top) ?? 0;
            return absolutePos - referencePos;
        }
        get length() {
            const button = this.menuButtons.find(element => element.current);
            return (this.portrait ? button?.getBoundingClientRect().width : button?.getBoundingClientRect().height) ?? 0;
        }
        onClick(e) {
            this.menuButtons.forEach(button => { button.current = false; });
            e.target.current = true;
            this.current = e.target.id;
            this.requestUpdate();
            this.sxCurrentUpdated(this.current);
        }
        onSlotChange() {
            this.current = this.menuButtons.find(button => button.current)?.id;
            this.requestUpdate();
            this.sxCurrentUpdated(this.current);
        }
        render = (__runInitializers(this, _menuButtons_extraInitializers), () => html `
        <div id="base" ?portrait=${this.portrait} style=${styleMap({ "--pos": `${this.pos}px`, "--len": `${this.length}px` })}>
            <div id="nav-bar">
                ${when(!this.portrait && this.logo, () => html `
                    <div id="logo">
                        <img src=${this.logo}>
                    </div>
                `)}
                <div id="nav-menu">
                    <div id="nav-indicator"></div>
                    <slot 
                        name="menu"
                        @click=${this.onClick}
                        @slotchange=${this.onSlotChange}
                    ></slot>
                </div>
            </div>
            <div id="page">
                <slot name="page"></slot>
            </div>
        </div>
    `);
        // STYLE
        static styles = [
            css `
            :host {
                display: contents;
                --sx-menu-min-width: 10rem;
                --sx-background-image: none;
            }

            #base {
                box-sizing: border-box;
                width:100%;
                height:100%;
                display:flex;
                flex-direction: row;
                padding:1rem;
            }

            #base[portrait]{
                flex-direction: column;
                flex-flow: column-reverse;
            }

            #nav-bar{
                position: relative;
                display: grid;
                place-items: center stretch;
                border-right: 1px solid var(--sl-color-neutral-200);
                min-width: var(--sx-menu-min-width);
            }

            #base[portrait] #nav-bar{
                place-items: center center;
                border-top: 1px solid var(--sl-color-neutral-200);
                border-right:none;
                min-width: 0;
            }

           #nav-menu {
                position: relative;
                display: flex;
                flex-direction: column;
                align-items: stretch;
            }

            #nav-indicator {
                position: absolute;
                left:100%;
                width:2px;
                top: var(--pos);
                height: var(--len);
                background-color: var(--sl-color-primary-600);
                transition: all var(--sl-transition-fast) ease;
            }

            #base[portrait] #nav-indicator {
                left: var(--pos);
                height:2px;
                top:0;
                width:var(--len);
            }

            #base[portrait] #nav-menu {
                flex-direction: row;
            }

            #page {
                height:100%;
                flex-grow: 1;
                background-image: var(--sx-background-image);
                background-size: cover;
                background-position: center;
                display:grid;
                place-items: stretch;
                padding-inline: 1rem;
            }

            #logo {
                position:absolute;
                width: 100%;
                height:100%;
                display:grid;
                place-items: start center;
            }

            #logo>img {
                width:100%;
            }

        `
        ];
        static {
            __runInitializers(_classThis, _classExtraInitializers);
        }
    });
    return _classThis;
})();

let SxShellButton = (() => {
    let _classDecorators = [customElement('sx-shell-button')];
    let _classDescriptor;
    let _classExtraInitializers = [];
    let _classThis;
    let _classSuper = LitElement;
    let _icon_decorators;
    let _icon_initializers = [];
    let _icon_extraInitializers = [];
    let _current_decorators;
    let _current_initializers = [];
    let _current_extraInitializers = [];
    let _portrait_decorators;
    let _portrait_initializers = [];
    let _portrait_extraInitializers = [];
    (class extends _classSuper {
        static { _classThis = this; }
        static {
            const _metadata = typeof Symbol === "function" && Symbol.metadata ? Object.create(_classSuper[Symbol.metadata] ?? null) : void 0;
            _icon_decorators = [property({ type: String })];
            _current_decorators = [property({ type: Boolean })];
            _portrait_decorators = [mediaQuery("(orientation: portrait)")];
            __esDecorate(this, null, _icon_decorators, { kind: "accessor", name: "icon", static: false, private: false, access: { has: obj => "icon" in obj, get: obj => obj.icon, set: (obj, value) => { obj.icon = value; } }, metadata: _metadata }, _icon_initializers, _icon_extraInitializers);
            __esDecorate(this, null, _current_decorators, { kind: "accessor", name: "current", static: false, private: false, access: { has: obj => "current" in obj, get: obj => obj.current, set: (obj, value) => { obj.current = value; } }, metadata: _metadata }, _current_initializers, _current_extraInitializers);
            __esDecorate(null, null, _portrait_decorators, { kind: "field", name: "portrait", static: false, private: false, access: { has: obj => "portrait" in obj, get: obj => obj.portrait, set: (obj, value) => { obj.portrait = value; } }, metadata: _metadata }, _portrait_initializers, _portrait_extraInitializers);
            __esDecorate(null, _classDescriptor = { value: _classThis }, _classDecorators, { kind: "class", name: _classThis.name, metadata: _metadata }, null, _classExtraInitializers);
            _classThis = _classDescriptor.value;
            if (_metadata) Object.defineProperty(_classThis, Symbol.metadata, { enumerable: true, configurable: true, writable: true, value: _metadata });
        }
        #icon_accessor_storage = __runInitializers(this, _icon_initializers, void 0);
        // API
        /**
         * Name of the prefix icon
         */
        get icon() { return this.#icon_accessor_storage; }
        set icon(value) { this.#icon_accessor_storage = value; }
        #current_accessor_storage = (__runInitializers(this, _icon_extraInitializers), __runInitializers(this, _current_initializers, false));
        /**
         * Indicate whether this button is the current one
         */
        get current() { return this.#current_accessor_storage; }
        set current(value) { this.#current_accessor_storage = value; }
        // INTERNAL
        portrait = (__runInitializers(this, _current_extraInitializers), __runInitializers(this, _portrait_initializers, void 0));
        // TEMPLATE
        render = (__runInitializers(this, _portrait_extraInitializers), () => html `
        <div id="base" ?portrait=${this.portrait} ?current=${this.current}>
            ${when(this.icon, () => html `
                <sl-icon name=${this.icon}></sl-icon>
            `)}
            <slot></slot>
        </div>
    `);
        // STYLE
        static styles = [
            css `
            :host {
                display: inline-block;
            }

            #base {
                display: flex;
                gap: 1rem;
                align-items: center;
                padding: var(--sl-spacing-medium) var(--sl-spacing-large);
                font-size: var(--sl-font-size-small);
                font-weight: var(--sl-font-weight-semibold);
                color: var(--sl-color-neutral-600);
                cursor: pointer;
            }

            #base:hover {
                color: var(--sl-color-primary-500);
            }

            #base[current] {
                color: var(--sl-color-primary-600);
            }

            #base[portrait]{
                flex-direction: column;
                gap:0.5rem;
            }

            sl-icon{
                font-size: 1.3rem;
                color: var(--sl-color-primary-400);
            }
        `
        ];
        static {
            __runInitializers(_classThis, _classExtraInitializers);
        }
    });
    return _classThis;
})();

let SxPage = (() => {
    let _classDecorators = [customElement('sx-page')];
    let _classDescriptor;
    let _classExtraInitializers = [];
    let _classThis;
    let _classSuper = LitElement;
    let _title_decorators;
    let _title_initializers = [];
    let _title_extraInitializers = [];
    let _back_decorators;
    let _back_initializers = [];
    let _back_extraInitializers = [];
    let _sxBack_decorators;
    let _sxBack_initializers = [];
    let _sxBack_extraInitializers = [];
    (class extends _classSuper {
        static { _classThis = this; }
        static {
            const _metadata = typeof Symbol === "function" && Symbol.metadata ? Object.create(_classSuper[Symbol.metadata] ?? null) : void 0;
            _title_decorators = [property({ type: String })];
            _back_decorators = [property({ type: String })];
            _sxBack_decorators = [event("sx-back")];
            __esDecorate(this, null, _title_decorators, { kind: "accessor", name: "title", static: false, private: false, access: { has: obj => "title" in obj, get: obj => obj.title, set: (obj, value) => { obj.title = value; } }, metadata: _metadata }, _title_initializers, _title_extraInitializers);
            __esDecorate(this, null, _back_decorators, { kind: "accessor", name: "back", static: false, private: false, access: { has: obj => "back" in obj, get: obj => obj.back, set: (obj, value) => { obj.back = value; } }, metadata: _metadata }, _back_initializers, _back_extraInitializers);
            __esDecorate(null, null, _sxBack_decorators, { kind: "field", name: "sxBack", static: false, private: false, access: { has: obj => "sxBack" in obj, get: obj => obj.sxBack, set: (obj, value) => { obj.sxBack = value; } }, metadata: _metadata }, _sxBack_initializers, _sxBack_extraInitializers);
            __esDecorate(null, _classDescriptor = { value: _classThis }, _classDecorators, { kind: "class", name: _classThis.name, metadata: _metadata }, null, _classExtraInitializers);
            _classThis = _classDescriptor.value;
            if (_metadata) Object.defineProperty(_classThis, Symbol.metadata, { enumerable: true, configurable: true, writable: true, value: _metadata });
        }
        #title_accessor_storage = __runInitializers(this, _title_initializers, void 0);
        // API
        /**
         * Title of the page
         */
        get title() { return this.#title_accessor_storage; }
        set title(value) { this.#title_accessor_storage = value; }
        #back_accessor_storage = (__runInitializers(this, _title_extraInitializers), __runInitializers(this, _back_initializers, void 0));
        /**
         * Name of the icon of the back button
         * No back button is diplayed when undefined.
         */
        get back() { return this.#back_accessor_storage; }
        set back(value) { this.#back_accessor_storage = value; }
        /**
         * Event raised when the back button is clicked
         */
        sxBack = (__runInitializers(this, _back_extraInitializers), __runInitializers(this, _sxBack_initializers, void 0));
        // TEMPLATE
        render = (__runInitializers(this, _sxBack_extraInitializers), () => html `
        <div id="header">
            ${when(this.back, () => html `
                <sl-button @click=${this.sxBack} variant="default" size="medium" circle>
                    <sl-icon name=${this.back}></sl-icon>
                </sl-button>
            `)}
            <div id="title">${this.title}</div>
        </div>
        <div id="content">
            <div id="overflow">
                <slot></slot>
            </div>
        </div>
        `);
        // STYLE
        static styles = [
            css `
            :host {
                display: grid;
                grid-template-rows: auto 1fr;
                place-items: stretch;
            }

            #header {
                display: flex;
                align-items: stretch;
                gap: 2rem;
            }

            #title {
                font-size: var(--sl-font-size-x-large);
                color: var(--sl-color-primary-500);
                padding-bottom: 1rem;
                border-bottom: 1px solid var(--sl-color-gray-300);
                flex-grow: 1;
            }

            #content {
                position:relative
            }

            #overflow {
                position: absolute;
                height:100%;
                width:100%;
                display: grid;
                place-items: center;
                overflow-y: auto;
                padding-block:1rem;
                box-sizing: border-box;
            }
        `
        ];
        static {
            __runInitializers(_classThis, _classExtraInitializers);
        }
    });
    return _classThis;
})();

function registerIcons(path) {
    registerIconLibrary('default', {
        resolver: name => `${path}/${name}.svg`,
        mutator: svg => svg.setAttribute('fill', 'currentColor')
    });
}

export { SxMarkdown, SxPage, SxShell, SxShellButton, extendForms, registerIcons };
//# sourceMappingURL=index.js.map
