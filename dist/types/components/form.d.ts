/**
 * Custom event that contains all form data.
 * Is raised when :
 * - a submit event occurs on the form
 * - form validation constraints are satisfied.
 */
export type SxSubmitEvent = CustomEvent<Record<string, unknown>>;
/**
 * Extends forms under the given shadowRoot so that they raise SxSubmitEvents.
 *
 * For now it should be called in "updated" callback until this issue is fixed:
 * https://github.com/shoelace-style/shoelace/issues/718
 *
 * TLDR: onFormSubmit must be added after all form inputs are added to DOM
 * Once resolved we should either move this in a mutation observer or use directly onSubmit in forms
 */
export declare function extendForms(root: ShadowRoot): void;
//# sourceMappingURL=form.d.ts.map