import { LitElement } from 'lit';
/**
 * This element provides a markdown renderer.
 */
export declare class SxMarkdown extends LitElement {
    /**
     * Markdown string to be rendered
     */
    accessor content: string;
    protected get unsafeHtml(): import("lit-html/directive.js").DirectiveResult<typeof import("lit-html/directives/unsafe-html.js").UnsafeHTMLDirective>;
    protected render: () => import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
//# sourceMappingURL=markdown.d.ts.map