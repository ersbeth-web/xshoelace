import { LitElement } from 'lit';
import { type EventEmitter } from "@ersbeth/xlit";
export declare class SxShell extends LitElement {
    /**
     * Path to the logo image
     */
    accessor logo: string | undefined;
    /**
     * Current button id
     */
    accessor current: string | undefined;
    sxCurrentUpdated: EventEmitter<string>;
    protected portrait: boolean;
    protected accessor menu: HTMLDivElement;
    protected accessor menuButtons: Array<HTMLElement>;
    protected get pos(): number;
    protected get length(): number;
    protected onClick(e: Event): void;
    protected onSlotChange(): void;
    protected render: () => import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
//# sourceMappingURL=shell.d.ts.map