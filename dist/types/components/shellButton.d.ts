import { LitElement } from 'lit';
export declare class SxShellButton extends LitElement {
    /**
     * Name of the prefix icon
     */
    accessor icon: string | undefined;
    /**
     * Indicate whether this button is the current one
     */
    accessor current: boolean;
    protected portrait: boolean;
    protected render: () => import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
//# sourceMappingURL=shellButton.d.ts.map