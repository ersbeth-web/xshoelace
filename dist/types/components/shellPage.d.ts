import { LitElement } from 'lit';
import { type EventEmitter } from '@ersbeth/xlit';
export declare class SxPage extends LitElement {
    /**
     * Title of the page
     */
    accessor title: string;
    /**
     * Name of the icon of the back button
     * No back button is diplayed when undefined.
     */
    accessor back: string | undefined;
    /**
     * Event raised when the back button is clicked
     */
    sxBack: EventEmitter<void>;
    protected render: () => import("lit-html").TemplateResult<1>;
    static styles: import("lit").CSSResult[];
}
//# sourceMappingURL=shellPage.d.ts.map