import { nodeResolve } from "@rollup/plugin-node-resolve";
import typescript from "@rollup/plugin-typescript";
import copy from 'rollup-plugin-copy'

export default {
    input: "./src/index.ts",
    plugins: [
        typescript({
            exclude: "./test/**/*",
            rootDir: "./src",
        }),
        nodeResolve(),
        copy({
            targets: [
                { src: 'src/css/global.css', dest: 'dist/css' },
            ]
        })
    ],
    external: [ // we cant use /node_module/ because it excludes tslib too.
        /lit/,
        /marked/,
        /@shoelace-style/
    ],
    output: [
        {
            file: `./dist/index.js`,
            format: "esm",
            sourcemap: true
        },
    ],
}

