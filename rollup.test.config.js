import { nodeResolve } from "@rollup/plugin-node-resolve";
import typescript from "@rollup/plugin-typescript";
import css from "rollup-plugin-import-css";

export default {
    input: "./test/index.ts",
    plugins: [
        typescript({
            exclude: "./src/**/*",
            declaration: false,
            declarationMap: false,
            noEmit: true,
        }),
        nodeResolve(),
        css({ output: 'build.css' }),
    ],
    output: [
        {
            file: `./test/build/build.js`,
            format: "esm",
            sourcemap: true
        },
    ],
}

