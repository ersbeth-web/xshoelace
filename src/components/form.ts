import { serialize } from '@shoelace-style/shoelace/dist/utilities/form.js';

/**
 * Custom event that contains all form data.
 * Is raised when :
 * - a submit event occurs on the form
 * - form validation constraints are satisfied.
 */
export type SxSubmitEvent = CustomEvent<Record<string, unknown>>

/** 
 * Extends forms under the given shadowRoot so that they raise SxSubmitEvents.
 * 
 * For now it should be called in "updated" callback until this issue is fixed:
 * https://github.com/shoelace-style/shoelace/issues/718
 * 
 * TLDR: onFormSubmit must be added after all form inputs are added to DOM 
 * Once resolved we should either move this in a mutation observer or use directly onSubmit in forms
 */
export function extendForms(root: ShadowRoot) {
    const forms = root.querySelectorAll("form");
    forms.forEach(form => form.removeEventListener("submit", onSubmit))
    forms.forEach(form => form.addEventListener("submit", onSubmit))
}

/**
 * This function prevents default form sending behaviour (page reload) and forwards form data as JS object.
 * @param event Submit event
 */
function onSubmit(event: Event) {
    event.preventDefault();
    const form = event.target as HTMLFormElement
    const data = serialize(form);
    form.dispatchEvent(new CustomEvent("sx-submit", { detail: data }))
}