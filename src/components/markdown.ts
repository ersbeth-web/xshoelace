import { LitElement, html, css } from 'lit';
import { property, customElement } from "lit/decorators.js";
import { unsafeHTML } from 'lit/directives/unsafe-html.js';
import { marked } from 'marked';

/**
 * This element provides a markdown renderer.
 */
@customElement('sx-markdown')
export class SxMarkdown extends LitElement {

    // API

    /**
     * Markdown string to be rendered
     */
    @property({ type: String }) accessor content: string = "";

    // INTERNAL

    protected get unsafeHtml() { return unsafeHTML(marked.parse(this.content) as string) }

    // TEMPLATE

    protected render = () => html`${this.unsafeHtml}`

    // STYLE

    static styles = [
        css`
        em  {
            color: var(--sl-color-primary-700);
        }

        strong {
            color: var(--sl-color-danger-600);
        }

        a {
            color: var(--sl-color-primary-500);
        }

        li {
            margin-bottom: 0.5rem;
        }

        p,li{
            line-height: 1.3em;
            font-size: 0.9rem;        
        }

        h1 {
            font-size: 1rem;
            color: var(--sl-color-primary-500);
            margin-block: 1.5rem 0.7rem;
            margin-inline: 0px;
            font-weight: bold;       
        }

        h2 {
            font-size: 0.8rem;
            color: var(--sl-color-primary-600);
            margin-block: 1rem 0.5rem;
            margin-inline: 0px;
            font-weight: bold;       
        }
        `
    ]
}