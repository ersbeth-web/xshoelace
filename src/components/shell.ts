import { LitElement, html, css } from 'lit';
import { customElement, property, query, queryAssignedElements, } from 'lit/decorators.js';
import { when } from 'lit/directives/when.js';
import { event, mediaQuery, type EventEmitter } from "@ersbeth/xlit";
import { styleMap } from 'lit/directives/style-map.js';
import type { SxShellButton } from './shellButton';

@customElement('sx-shell')
export class SxShell extends LitElement {

    /**
     * Path to the logo image
     */
    @property({ type: String }) accessor logo: string | undefined;

    /**
     * Current button id
     */
    @property({ type: String, }) accessor current: string | undefined;

    @event("sx-current-updated") sxCurrentUpdated!: EventEmitter<string>;

    // INTERNAL

    @mediaQuery("(orientation: portrait)") protected portrait!: boolean;
    @query("#nav-menu") protected accessor menu!: HTMLDivElement;
    @queryAssignedElements({ slot: "menu" }) protected accessor menuButtons!: Array<HTMLElement>;

    protected get pos() {
        const button = this.menuButtons.find(element => (element as SxShellButton).current);
        const absolutePos = (this.portrait ? button?.getBoundingClientRect().left : button?.getBoundingClientRect().top) ?? 0
        const referencePos = (this.portrait ? this.menu?.getBoundingClientRect().left : this.menu?.getBoundingClientRect().top) ?? 0
        return absolutePos - referencePos
    }

    protected get length() {
        const button = this.menuButtons.find(element => (element as SxShellButton).current);
        return (this.portrait ? button?.getBoundingClientRect().width : button?.getBoundingClientRect().height) ?? 0
    }

    protected onClick(e: Event) {
        this.menuButtons.forEach(button => { (button as SxShellButton).current = false });
        (e.target as SxShellButton).current = true;
        this.current = (e.target as SxShellButton).id;
        this.requestUpdate();
        this.sxCurrentUpdated(this.current);
    }

    protected onSlotChange() {
        this.current = (this.menuButtons.find(button => (button as SxShellButton).current) as SxShellButton)?.id
        this.requestUpdate();
        this.sxCurrentUpdated(this.current);
    }

    protected render = () => html`
        <div id="base" ?portrait=${this.portrait} style=${styleMap({ "--pos": `${this.pos}px`, "--len": `${this.length}px` })}>
            <div id="nav-bar">
                ${when(!this.portrait && this.logo, () => html`
                    <div id="logo">
                        <img src=${this.logo!}>
                    </div>
                `)}
                <div id="nav-menu">
                    <div id="nav-indicator"></div>
                    <slot 
                        name="menu"
                        @click=${this.onClick}
                        @slotchange=${this.onSlotChange}
                    ></slot>
                </div>
            </div>
            <div id="page">
                <slot name="page"></slot>
            </div>
        </div>
    `

    // STYLE

    static styles = [
        css`
            :host {
                display: contents;
                --sx-menu-min-width: 10rem;
                --sx-background-image: none;
            }

            #base {
                box-sizing: border-box;
                width:100%;
                height:100%;
                display:flex;
                flex-direction: row;
                padding:1rem;
            }

            #base[portrait]{
                flex-direction: column;
                flex-flow: column-reverse;
            }

            #nav-bar{
                position: relative;
                display: grid;
                place-items: center stretch;
                border-right: 1px solid var(--sl-color-neutral-200);
                min-width: var(--sx-menu-min-width);
            }

            #base[portrait] #nav-bar{
                place-items: center center;
                border-top: 1px solid var(--sl-color-neutral-200);
                border-right:none;
                min-width: 0;
            }

           #nav-menu {
                position: relative;
                display: flex;
                flex-direction: column;
                align-items: stretch;
            }

            #nav-indicator {
                position: absolute;
                left:100%;
                width:2px;
                top: var(--pos);
                height: var(--len);
                background-color: var(--sl-color-primary-600);
                transition: all var(--sl-transition-fast) ease;
            }

            #base[portrait] #nav-indicator {
                left: var(--pos);
                height:2px;
                top:0;
                width:var(--len);
            }

            #base[portrait] #nav-menu {
                flex-direction: row;
            }

            #page {
                height:100%;
                flex-grow: 1;
                background-image: var(--sx-background-image);
                background-size: cover;
                background-position: center;
                display:grid;
                place-items: stretch;
                padding-inline: 1rem;
            }

            #logo {
                position:absolute;
                width: 100%;
                height:100%;
                display:grid;
                place-items: start center;
            }

            #logo>img {
                width:100%;
            }

        `
    ];
}