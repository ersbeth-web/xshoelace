import { LitElement, html, css } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { mediaQuery } from '@ersbeth/xlit';
import { when } from 'lit/directives/when.js';

@customElement('sx-shell-button')
export class SxShellButton extends LitElement {

    // API

    /**
     * Name of the prefix icon
     */
    @property({ type: String }) accessor icon: string | undefined;

    /**
     * Indicate whether this button is the current one
     */
    @property({ type: Boolean }) accessor current: boolean = false;

    // INTERNAL

    @mediaQuery("(orientation: portrait)") protected portrait!: boolean;

    // TEMPLATE

    protected render = () => html`
        <div id="base" ?portrait=${this.portrait} ?current=${this.current}>
            ${when(this.icon, () => html`
                <sl-icon name=${this.icon!}></sl-icon>
            `)}
            <slot></slot>
        </div>
    `

    // STYLE

    static styles = [
        css`
            :host {
                display: inline-block;
            }

            #base {
                display: flex;
                gap: 1rem;
                align-items: center;
                padding: var(--sl-spacing-medium) var(--sl-spacing-large);
                font-size: var(--sl-font-size-small);
                font-weight: var(--sl-font-weight-semibold);
                color: var(--sl-color-neutral-600);
                cursor: pointer;
            }

            #base:hover {
                color: var(--sl-color-primary-500);
            }

            #base[current] {
                color: var(--sl-color-primary-600);
            }

            #base[portrait]{
                flex-direction: column;
                gap:0.5rem;
            }

            sl-icon{
                font-size: 1.3rem;
                color: var(--sl-color-primary-400);
            }
        `
    ];
}