import { LitElement, html, css } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { when } from 'lit/directives/when.js';
import { event, type EventEmitter } from '@ersbeth/xlit';

@customElement('sx-page')
export class SxPage extends LitElement {

    // API

    /**
     * Title of the page
     */
    @property({ type: String }) accessor title!: string;

    /**
     * Name of the icon of the back button
     * No back button is diplayed when undefined.
     */
    @property({ type: String }) accessor back: string | undefined;

    /**
     * Event raised when the back button is clicked
     */
    @event("sx-back") sxBack!: EventEmitter<void>;

    // TEMPLATE

    protected render = () => html`
        <div id="header">
            ${when(this.back, () => html`
                <sl-button @click=${this.sxBack} variant="default" size="medium" circle>
                    <sl-icon name=${this.back!}></sl-icon>
                </sl-button>
            `)}
            <div id="title">${this.title}</div>
        </div>
        <div id="content">
            <div id="overflow">
                <slot></slot>
            </div>
        </div>
        `

    // STYLE

    static styles = [
        css`
            :host {
                display: grid;
                grid-template-rows: auto 1fr;
                place-items: stretch;
            }

            #header {
                display: flex;
                align-items: stretch;
                gap: 2rem;
            }

            #title {
                font-size: var(--sl-font-size-x-large);
                color: var(--sl-color-primary-500);
                padding-bottom: 1rem;
                border-bottom: 1px solid var(--sl-color-gray-300);
                flex-grow: 1;
            }

            #content {
                position:relative
            }

            #overflow {
                position: absolute;
                height:100%;
                width:100%;
                display: grid;
                place-items: center;
                overflow-y: auto;
                padding-block:1rem;
                box-sizing: border-box;
            }
        `
    ];
}