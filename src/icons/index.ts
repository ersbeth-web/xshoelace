import { registerIconLibrary } from '@shoelace-style/shoelace/dist/utilities/icon-library.js';

export function registerIcons(path: string) {

    registerIconLibrary('default', {
        resolver: name => `${path}/${name}.svg`,
        mutator: svg => svg.setAttribute('fill', 'currentColor')
    });

}
