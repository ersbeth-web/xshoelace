// LIT ELEMENT
import { LitElement, html } from 'lit';
import { customElement, query } from 'lit/decorators.js';
import { when } from "lit/directives/when.js";

// SHOELACE
import '@shoelace-style/shoelace/dist/themes/light.css';
import '@shoelace-style/shoelace/dist/components/input/input.js';
import '@shoelace-style/shoelace/dist/components/button/button.js';
import '@shoelace-style/shoelace/dist/components/icon/icon.js';

// X-SHOELACE
import { extendForms, SxShell, registerIcons, type SxSubmitEvent } from "@ersbeth/xshoelace"
import "@ersbeth/xshoelace/dist/css/global.css"

registerIcons("./icons");

@customElement('x-app')
export class App extends LitElement {

    @query("sx-shell") accessor shell!: SxShell

    mdString =
        `# Title
## Subtitle
Unordered list:
* Bullet 1
* Bullet 2

Ordered list:
1. Bullet 1
2. Bullet 2

Lorem ipsum *ldolor* sit **amet**, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
`
    protected updated(): void { extendForms(this.shadowRoot!); }

    submit(e: SxSubmitEvent) {
        console.log(e.detail);
    }

    back() {
        console.log("back");
    }


    protected render = () => {
        return html`
        <sx-shell logo="./logo.png" @sx-current-updated=${() => this.requestUpdate()}>

            <sx-shell-button id="markdown" slot="menu" icon="calendar-days-solid" current>Markdown</sx-shell-button>
            <sx-shell-button id="form" slot="menu" icon="person-solid">Forms</sx-shell-button>
            <sx-shell-button id="settings" slot="menu" icon="gear-solid">Settings</sx-shell-button>

            ${when(this.shell?.current == "markdown", () => html`
                <sx-page slot="page" title="Markdown" back="arrow-left-solid" @sx-back=${this.back}>
                    <sx-markdown .content=${this.mdString}></sx-markdown>
                </sx-page>
            `)}

            ${when(this.shell?.current == "form", () => html`
                <sx-page slot="page" title="Form">
                    <form id="sxForm" @sx-submit=${this.submit}>
                        <sl-input name="input1" label="input1"></sl-input>
                    </form>
                    <sl-button type="submit" form="sxForm">Submit</sl-button>
                </sx-page>
            `)}

            ${when(this.shell?.current == "settings", () => html`
                <sx-page slot="page" title="Settings" back="arrow-left-solid" @sx-back=${this.back}>
                    Settings page
                </sx-page>
            `)}

        </sx-shell>

                  `
    }
}

const element = document.createElement('x-app');
document.body.appendChild(element);
